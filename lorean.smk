


def to_bash_array(l):
    quoted = (f'"{e}"' for e in l)
    return "(" + " ".join(quoted) + ")"



rule lorean:
    input:
        fasta=config['fasta'],
        long_reads=config['long_reads'],
        mask=config['mask']
    output:
        config['prefix'] + "_lorean_results.tar.gz",
        config["prefix"] + "_lorean_read_mappings.bam"
    threads:
        32
    shell:
        """
        wget https://salk-tm-pub.s3-us-west-2.amazonaws.com/lorean_files/config.augustus.tar.gz
        tar -zxf config.augustus.tar.gz
        wget https://salk-tm-pub.s3-us-west-2.amazonaws.com/lorean_files/RepeatMasker.Libraries.tar.gz
        tar -zxf RepeatMasker.Libraries.tar.gz
        rm RepeatMasker.Libraries.tar.gz config.augustus.tar.gz
        wget https://ftp.uniprot.org/pub/databases/uniprot/current_release/knowledgebase/complete/uniprot_sprot.fasta.gz
        gunzip uniprot_sprot.fasta.gz
        echo TCCACGATCTAGACTCGTAGGATCTCGTAATATCCATGTTTTTTTTTTTTTTTTCACTTCTGACTAACCAGTCAGTTGGG > ~/.gm_key
        echo 571543565 >> ~/.gm_key
    
        singularity pull docker://lfaino/lorean:latest
        zcat {input.long_reads} > long_reads.fastq

        set +e
        set +u
        singularity exec \\
            -B config/:/opt/LoReAn/third_party/software/augustus/config/ \\
            -B Libraries/:/usr/local/RepeatMasker/Libraries/ \\
            -B $(pwd):$(pwd) \\
            lorean_latest.sif lorean -mm -t {threads} -sp delorean \\
            -lr $(pwd)/long_reads.fastq -pr $(pwd)/uniprot_sprot.fasta \\
            -o $(pwd)/lorean_results $(pwd)/{input.fasta} -rp {input.mask}
        set -e
        set -u
        mv lorean_results_output/long_reads_mapped.bam.sorted.bam {output[1]}
    
        tar -zcf {output[0]} lorean_results_output/
        """


def braker_input():
    ret = [config['fasta']]
    if(len(config["long_reads"]) > 0):
        ret.append(config["long_reads"])
    if(len(config['short_reads']) > 0):
        ret.append([p['r1'] for p in config["short_reads"]])
        ret.append([p['r2'] for p in config["short_reads"]])
    return ret


rule braker:
    input:
        braker_input()
    output:
        config["prefix"] + "_braker_results.tar.gz"
    params:
        r1_read_array = to_bash_array([p['r1'] for p in config["short_reads"]]),
        r2_read_array = to_bash_array([p['r2'] for p in config["short_reads"]]),
        long_read_array = to_bash_array(config["long_reads"]),
        utr_flag = config.get("utr_flag", "off")
    threads:
        config.get("threads", 16)
    conda:
        "envs/braker.yml"
    shell:
        """
        # install genemark
        wget https://salk-tm-pub.s3-us-west-2.amazonaws.com/lorean_files/gmes_linux_64_4.enved.tar.gz
        tar -zxf gmes_linux_64_4.enved.tar.gz
        export PATH=$(pwd)/gmes_linux_64_4/:$PATH
        export PATH=$(pwd)/gmes_linux_64_4/ProtHint/bin/:$PATH
        echo TCCACGATCTAGACTCGTAGGATCTCGTAATATCCATGTTTTTTTTTTTTTTTTCACTTCTGACTAACCAGTCAGTTGGG > ~/.gm_key
        echo 571543565 >> ~/.gm_key

        # build read_arrays
        r1_reads={params.r1_read_array}
        r2_reads={params.r2_read_array}
        long_reads={params.long_read_array}

        # map_short_read_data
        if [ ${{#r1_reads[@]}} -gt 0 ] ; then
            hisat2-build {input[0]} hisat2_index
            for i in ${{!r1_reads[@]}} ; do \\
                hisat2 -p {threads} -x hisat2_index \\
                    -1 ${{r1_reads[$i]}} -2 ${{r2_reads[$i]}} | samtools sort > reads.short.$i.hst2.bam \\
            ; done
        fi

        # map long read data
        if [ ${{#long_reads[@]}} -gt 0 ] ; then
            for i in ${{!long_reads[@]}} ; do \\
                minimap2 -t {threads} -ax splice {input[0]} ${{long_reads[$i]}} | samtools sort > reads.long.$i.hst2.bam \\
            ; done
        fi
    
        # merge all bams
        samtools merge all_alignments.bam reads.*.bam

        # run braker
        mkdir braker_results/
        cd braker_results
        braker.pl --genome=../{input[0]} \\
            --bam=../all_alignments.bam \\
            --softmasking --cores={threads} \\
            --UTR={params.utr_flag}
        cd ../
        tar -zcf {output} braker_results/
        """


rule braker_prot:
    input:
        ref = config["fasta"],
        prots = config["proteins"]
    output:
        config["prefix"] + "_braker_prot_results.tar.gz"
    threads:
        config.get("threads", 16)
    conda:
        "envs/braker.yml"
    shell:
        """
        # install genemark
        wget https://salk-tm-pub.s3-us-west-2.amazonaws.com/lorean_files/gmes_linux_64_4.enved.tar.gz
        tar -zxf gmes_linux_64_4.enved.tar.gz
        export PATH=$(pwd)/gmes_linux_64_4/:$PATH
        export PATH=$(pwd)/gmes_linux_64_4/ProtHint/bin/:$PATH
        echo TCCACGATCTAGACTCGTAGGATCTCGTAATATCCATGTTTTTTTTTTTTTTTTCACTTCTGACTAACCAGTCAGTTGGG > ~/.gm_key
        echo 571543565 >> ~/.gm_key
    
        # run braker
        mkdir braker_results/
        cd braker_results
        braker.pl --genome=../{input.ref} \\
            --softmasking --cores={threads} \\
            --prot_seq=../{input.prots}
        cd ../
        tar -zcf {output} braker_results/
        """


rule repeatmodeler:
    input:
        config["fasta"]
    output:
        config["prefix"] + "repeat_modeler.tar.gz"
    conda:
        "envs/repeatmodeler.yml"
    threads:
        config.get("threads", 32)
    shell:
        """
        mkdir repeatmodeler
        cd repeatmodeler
        BuildDatabase -name rmdb ../{input}
        RepeatModeler -database rmdb -pa {threads} -LTRStruct >& run.out
        cd ../
        tar -zcf {output} repeatmodeler
        """

