from BCBio import GFF
from Bio import SeqIO
import pandas
from itertools import groupby
from functools import partial
from collections import Counter
import os
import shutil
import numpy
import pronto


def load_csv(fpath, comment="#", **kargs):
    """ reads csv data found at fpath, returns header comments and a
        dataframe with the data. A comment is any line that starts with
        the comment string. The header is a list of comment lines that
        appear at the start of the file, if any. 
    """
    with open(fpath) as fin:
        header = []
        for line in fin:
            if(line.startswith(comment)):
                header.append(line)
            else:
                break
    df = pandas.read_csv(fpath, skiprows=len(header), comment=comment, **kargs)
    return header, df


def load_gff3(fpath):
    with open(fpath) as fin:
        ret = []
        for rec in GFF.parse(fin):
            ret.append(rec)
    return ret


def write_gff3(fpath, gff_data):
    with open(fpath, "w") as fin:
        GFF.write(gff_data, fin)


def load_fasta(f):
    """ reads fasta data located at f and returns a dataframe of shape 3xN
        where N is the number of entries in the fasta file. The columns are:
        ID, INFO, SEQ. 
    """
    with open(f) as fin:
        grouper = groupby(fin, lambda l: l[0]==">")
        grouper = ("".join(v) for k, v in grouper)
        data = {h[1:-1]: v.replace("\n", "") for h, v in zip(grouper, grouper)}
    ret = pandas.Series(data).to_frame().reset_index()
    ret.columns = ["HEADER", "SEQ"]
    ret["ID"] = ret.HEADER.str.split().str[0]
    ret["INFO"] = ret.HEADER.str.split().str[1:].str.join(" ")
    return ret[["ID", "INFO", "SEQ"]]


def write_fasta(fpath, df):
    """ Given a dataframe with fasta columns, write the data to fpath in fasta
        format. Dataframe should have columns named ID, INFO, and SEQ. Each
        row represents an entry in the resultant multi-fasta file
    """
    headers = (">" + df.ID + " " + df.INFO).str.rstrip()
    seqs = df.SEQ.str.wrap(80)
    with open(fpath, "w") as fout:
        for h, s in zip(headers, seqs):
            fout.write(f"{h}\n{s}\n")


def load_annotations(fpath):
    """ Reads a table output by emapper. Returns comments at top of file
        and a dataframe representing the rest. Returns (header, data) where
        header is a list of comment lines excluding the line that labels the
        columns and data is a dataframe representing the data in the file
    """
    header, df = load_csv(fpath, sep="\t", header=None)
    cols = header[-1][1:-1].split("\t")
    df.columns = cols
    return header[:-1], df


def write_annotations(fpath, header, df):
    """ Writes an emapper style table to fpath.
    """
    with open(fpath, "w") as fout:
        # fout.write("".join(header))
        # fout.write("#")
        df.to_csv(fout, sep="\t", index=False)


def make_gene_id_maps(gff_data, prefix):
    ret = {}
    nfeatures = max(len(r.features) for r in gff_data)
    pad = int(numpy.log10(nfeatures))+1
    formats = {
        "gene": "{prefix}.{ctg}.g{n}0",
        "mRNA": "{gid}_p{n}",
        "tRNA": "{gid}_tRNA{n}",
        "exon": "{tid}.e{n}",
        "CDS": "{tid}.cds"
    }
    for sr in gff_data:
        for i, g in enumerate(sr.features):
            gid = formats[g.type].format(prefix=prefix, ctg=sr.id, n=str(i+1).zfill(pad))
            ret[g.id] = gid
            for j, t in enumerate(g.sub_features):
                tid = formats[t.type].format(gid=gid, n=j+1)
                ret[t.id] = tid
                exons = [f for f in t.sub_features if(f.type == "exon")]
                cds = [f for f in t.sub_features if(f.type == "CDS")]
                for k, e in enumerate(exons):
                    ret[e.id] = formats[e.type].format(tid=tid, n=k+1)
                for k, c in enumerate(cds):
                    ret[c.id] = formats[c.type].format(tid=tid)
    return ret


def update_fasta(mapper, inpath, outpath):
    data = load_fasta(inpath)
    data.ID = data.ID.apply(mapper.get)
    data.INFO = data.INFO.apply(mapper.get)
    write_fasta(outpath, data)


def update_gff3(gff_data, mapper, annots, outpath):
    annots = annots.set_index("query_name")
    annots = {
        "description": annots["eggNOG free text desc."].dropna().to_dict(),
        "Ontology_term": annots["GOs"].dropna().str.split(",").to_dict(),
        "Dbxref": annots["KEGG_ko"].dropna().to_dict(),
        "best_go": annots["best_GO"].dropna().to_dict(),
        "best_go_name": annots["best_GO_name"].dropna().to_dict()
    }
    features = [f for s in gff_data for f in s.features]
    while(len(features) > 0):
        f = features.pop()
        f.id = mapper[f.id]
        f.qualifiers["ID"] = [f.id]
        if("Parent" in f.qualifiers):
            f.qualifiers["Parent"] = []
        for a, d in annots.items():
            if(f.id in d):
                f.qualifiers[a] = d[f.id]
        for sub in f.sub_features:
            features.append(sub)
    write_gff3(outpath, gff_data)


class nullgo:
    name = None


def main(
    prefix, obo, genome, gene_gff3, annots, prots,
    transcripts, cds, repeats, repeats_gff3, repeats_sum
):
    """ org_code and ass_version are strings. The rest are tuples specifying
        input and output file paths
    """
    genes = load_gff3(gene_gff3[0])
    id_maps = make_gene_id_maps(genes, prefix)
    shutil.copy(*genome)
    shutil.copy(*repeats)
    shutil.copy(*repeats_gff3)
    shutil.copy(*repeats_sum)
    update_fasta(id_maps, *cds)
    update_fasta(id_maps, *prots)
    update_fasta(id_maps, *transcripts)
    prot_data = load_fasta(prots[0])
    prot_data = {i: s for i, s in zip(prot_data.ID, prot_data.SEQ)}
    header, annot_data = load_annotations(annots[0])
    missing_prots = set(prot_data) - set(annot_data.query_name)
    missing_prots = pandas.DataFrame({"query_name": list(missing_prots)})
    annot_data = pandas.concat([annot_data, missing_prots], ignore_index=True)
    annot_data = annot_data.sort_values("query_name")
    annot_data["protein_sequence"] = annot_data.query_name.apply(prot_data.get)
    godata = pronto.Ontology(obo)
    gocounts = Counter([g for gos in annot_data.GOs.dropna() for g in gos.split(",")])
    gocounts = {
        go: count if(go in godata) else len(annot_data) + count
        for go, count in gocounts.items()
    }
    goidxes = list(annot_data.GOs.dropna().index)
    annot_data.loc[goidxes, "best_GO"] = annot_data.GOs.dropna().str.split(",").apply(
        lambda gos: sorted(gos, key=gocounts.get)[0]
    )
    annot_data.loc[goidxes, "best_GO_name"] = annot_data.best_GO.dropna().apply(
        lambda go: godata.get(go, nullgo).name
    )
    annot_data.query_name = annot_data.query_name.apply(id_maps.get)
    write_annotations(annots[1], header, annot_data)
    update_gff3(genes, id_maps, annot_data, gene_gff3[1])
    


def snakemain():
    io = {k: (snakemake.input[k], snakemake.output[k]) for k in snakemake.input.keys()}
    obo = snakemake.params["obo"]
    prefix = snakemake.params["prefix"]
    outdir = snakemake.params["outdir"]
    os.makedirs(outdir, exist_ok=True)
    main(prefix, obo, **io)


if(__name__ == "__main__"):
    snakemain()
