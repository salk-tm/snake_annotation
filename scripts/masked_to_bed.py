import finalize_names


def main(fasta, outbed):
    df = finalize_names.load_fasta(fasta)
    with open(outbed, "w") as fout:
        for fid, seq in zip(df.ID, df.SEQ):
            isN = False
            for i, c in enumerate(seq):
                if(c == "N" and not isN):
                    start = i
                    isN = True
                if(c != "N" and isN):
                    fout.write(f"{fid}\t{start}\t{i}\n")
                    isN = False
            if(isN):
                fout.write(f"{fid}\t{start}\t{len(seq) - 1}\n")


def snakemain():
    masked_fasta = snakemake.input[0]
    outbed = snakemake.output[0]
    main(masked_fasta, outbed)


if(__name__ == "__main__"):
    snakemain()
