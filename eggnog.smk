

prots = config["prots"]
eg_opt = config.get("eggnog_opts", "")

rule eggnogmapper:
    input:
        prots
    output:
        f"{prots}.emapper.annotations"
    params:
        out = prots,
        data_dir = "eggnog_data",
        opt = eg_opt
    conda:
        "envs/eggnog.yml"
    threads:
        config.get("threads", 16)
    shell:
        """
        mkdir -p {params[data_dir]}
        download_eggnog_data.py -y --data_dir {params[data_dir]}
        emapper.py {params[opt]} -i {input} -o {params[out]} -m diamond --cpu {threads} --data_dir {params[data_dir]}
        """
