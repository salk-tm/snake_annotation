import json
import os
from snakemake.utils import validate
import pandas

validate(config, "schema/config.schema.json")
print(json.dumps(config, indent=4))

bdb_path = os.path.join(workflow.basedir, "scripts", "busco_dbs.txt")
busco = pandas.read_csv(bdb_path, sep="\t", header=None, names=["db"])
busco["link"] = busco.db.apply("https://busco-data.ezlab.org/v4/data/lineages/{}".format)
busco["dirname"] = busco.db.str.split(".").str[0]
busco = busco.set_index("dirname")
print(busco)


def to_bash_array(l):
    quoted = (f'"{e}"' for e in l)
    return "(" + " ".join(quoted) + ")"



rule all:
    input:
         f"{config['prefix']}_final/{config['prefix']}.genome.fasta"


rule edta:
    input:
        config["fasta"]
    output:
        config["fasta"] + ".mod.EDTA.TElib.fa",
        config["fasta"] + ".mod.EDTA.TEanno.gff3",
        config["fasta"] + ".mod.EDTA.TEanno.sum",
    params:
        opt = config["opts"]["edta"]
    container:
        "docker://nolanhartwick/salk-containers:edta"
    threads:
        config["threads"]
    shell:
        "EDTA.pl {params[opt]} --genome {input} --anno 1 --threads {threads}"



rule trf:
    input:
        config["fasta"]
    output:
        config['fasta'] + ".trf.fasta"
    conda:
        "envs/general.yml"
    params:
        match = 1,
        mismatch = 1,
        delta = 2,
        pm = 80,
        pi = 5,
        minscore = 200,
        maxperiod = 2000,
    shell:
        """
        trf {input} {params.match} {params.mismatch} {params.delta} {params.pm} {params.pi} {params.minscore} {params.maxperiod} -m -d -h || true
        mv {input}.{params.match}.{params.mismatch}.{params.delta}.{params.pm}.{params.pi}.{params.minscore}.{params.maxperiod}.mask {output}
        """


rule trf_to_bed:
    input:
        rules.trf.output[0]
    output:
        config['fasta'] + ".trf.bed"
    conda:
        "envs/general.yml"
    script:
        "scripts/masked_to_bed.py"


rule maskfasta:
    input:
        config["fasta"],
        rules.edta.output[1],
        rules.trf_to_bed.output[0],
        rules.trf.output[0]
    output:
        f"{config['prefix']}.softmasked.fasta"
    conda:
        "envs/general.yml"
    threads:
        config['threads']
    shell:
        """
        bedtools maskfasta -fi {input[0]} -fo {input[1]}.softmasked.fasta -bed {input[1]} -soft
        bedtools maskfasta -fi {input[1]}.softmasked.fasta -fo {output} -bed {input[2]} -soft
        """


rule build_transcripts:
    input:
        reads=config["long_reads"],
        ref=config["fasta"]
    output:
        gtf=config["fasta"] + ".stringtie.gtf",
        bams=[f + ".mm2.bam" for f in config['long_reads']] if config['keep_lr_bams'] else []
    params:
        intron = config["intron_max"],
        opt_mm2 = config["opts"]["minimap2"],
        opt_str = config["opts"]["stringtie"]
    threads:
        config["threads"]
    conda:
        "envs/transcript_assembly.yml"
    shell:
        """
        for f in {input.reads}
        do
            minimap2 -ax splice {params[opt_mm2]} -G {params.intron} -t {threads} {input[ref]} $f | samtools sort > $f.mm2.bam
            stringtie {params[opt_str]} -L -p {threads} -v $f.mm2.bam -o $f.mm2.str.gtf
        done
        shopt -s globstar
        stringtie --merge -o {output.gtf} **/*.mm2.str.gtf
        """


def predict_input(wcs):
    inputs = [rules.maskfasta.output[0]]
    if(len(config["long_reads"]) > 0):
        inputs.append(rules.build_transcripts.output.gtf)
    inputs.extend([p['r1'] for p in config["short_reads"]])
    inputs.extend([p['r2'] for p in config["short_reads"]])
    inputs.extend(config["proteins"])
    if(len(config["other_gff"]) > 0):
        inputs.extend([d['gff'] for d in config["other_gff"]])
    return inputs


rule predict:
    input:
        predict_input
    output:
        predict_results = [
            f"fun_{config['prefix']}/predict_results/fun_{config['prefix']}.cds-transcripts.fa",
            f"fun_{config['prefix']}/predict_results/fun_{config['prefix']}.discrepency.report.txt",
            f"fun_{config['prefix']}/predict_results/fun_{config['prefix']}.error.summary.txt",
            f"fun_{config['prefix']}/predict_results/fun_{config['prefix']}.gbk",
            f"fun_{config['prefix']}/predict_results/fun_{config['prefix']}.gff3",
            f"fun_{config['prefix']}/predict_results/fun_{config['prefix']}.mrna-transcripts.fa",
            f"fun_{config['prefix']}/predict_results/fun_{config['prefix']}.proteins.fa",
            f"fun_{config['prefix']}/predict_results/fun_{config['prefix']}.scaffolds.fa",
            f"fun_{config['prefix']}/predict_results/fun_{config['prefix']}.validation.txt"
        ],
        logfiles = [
            f"fun_{config['prefix']}/logfiles/busco.log",
            f"fun_{config['prefix']}/logfiles/augustus-parallel.log",
            f"fun_{config['prefix']}/logfiles/funannotate-EVM.log",
            f"fun_{config['prefix']}/logfiles/funannotate-p2g.log",
            f"fun_{config['prefix']}/logfiles/funannotate-predict.log"
        ]
    container:
        "docker://nolanhartwick/salk-containers:fun"
    params:
        out = "fun_" + config["prefix"],
        name = config["prefix"],
        busco_link = busco.loc[config['busco']].link,
        busco_archive = busco.loc[config['busco']].db,
        busco_dir = config["busco"],
        predict_opt = config["opts"]["predict"],
        hst2_opt = config["opts"]["hisat2"],
        augustus_species = config["augustus_species"],
        fun_db = 'fun_db/',
        bam_ctrl = "" if(len(config["short_reads"]) + len(config["sra_short"])==0) else "--rna_bam alignments.bam",
        trans_ctrl = "" if(len(config["long_reads"]) == 0) else f"--stringtie {rules.build_transcripts.output.gtf}",
        max_intron_len = config["intron_max"],
        prots = "" if(len(config["proteins"]) == 0) else config["proteins"],
        other_gff = "--other_gff " + " ".join(f"{d['gff']}:{d['weight']}" for d in config['other_gff']) if(len(config["other_gff"]) > 0) else "",
        r1_read_array = to_bash_array([p['r1'] for p in config["short_reads"]] + [f"{n}_1.fastq" for n in config['sra_short']]),
        r2_read_array = to_bash_array([p['r2'] for p in config["short_reads"]] + [f"{n}_2.fastq" for n in config['sra_short']]),
        sra_array = to_bash_array(config["sra_short"]),
    threads:
        config["threads"]
    shell:    
        """        
        sra={params.sra_array}
        r1_reads={params.r1_read_array}
        r2_reads={params.r2_read_array}
        if [ ${{#r1_reads[@]}} -gt 0 ] ; then
            for n in ${{sra[@]}} ; do prefetch $n -o $n.sra && fasterq-dump $n.sra -o $n ; done
            hisat2-build {input[0]} hisat2_index
            for i in ${{!r1_reads[@]}} ; do \\
                hisat2 {params.hst2_opt} --max-intronlen {params.max_intron_len} -p {threads} -x hisat2_index \\
                    -1 ${{r1_reads[$i]}} -2 ${{r2_reads[$i]}} | samtools sort > reads.$i.hst2.bam \\
            ; done
            samtools merge alignments.bam reads.*.hst2.bam
        fi

        wget {params[busco_link]}
        tar -zxf {params[busco_archive]}
        funannotate setup -i uniprot -d {params[fun_db]}
        funannotate setup -i repeats -d {params[fun_db]}
        funannotate setup -i busco_outgroups -d {params[fun_db]}

        funannotate predict {params[predict_opt]} -i {input[0]} \\
            -o {params[out]} --species {params[out]} -d $(pwd)/{params[fun_db]} \\
            --busco_db $(pwd)/{params[busco_dir]} --organism other --repeats2evm \\
            --name {params[name]} --augustus_species {params[augustus_species]} \\
            --busco_seed_species {params[augustus_species]} --max_intronlen {params.max_intron_len} \\
            --protein_evidence $(pwd)/{params[fun_db]}uniprot_sprot.fasta {params[prots]} \\
            --cpus {threads} {params[bam_ctrl]} {params[trans_ctrl]} {params[other_gff]}
        """


rule eggnogmapper:
    input:
        rules.predict.output["predict_results"][6]
    output:
        f"{config['prefix']}.emapper.annotations"
    params:
        out = config['prefix'],
        data_dir = "eggnog_data",
        opt = config["opts"]["eggnog"]
    conda:
        "envs/eggnog.yml"
    threads:
        config["threads"]
    shell:
        """
        mkdir -p {params[data_dir]}
        download_eggnog_data.py -y --data_dir {params[data_dir]}
        emapper.py {params[opt]} -i {input} -o {params[out]} -m diamond --cpu {threads} --data_dir {params[data_dir]}
        """


rule finalize_names:
    input:
        genome = config["fasta"],
        gene_gff3 = rules.predict.output["predict_results"][4],
        prots = rules.predict.output["predict_results"][6],
        transcripts = rules.predict.output["predict_results"][5],
        cds = rules.predict.output["predict_results"][0],
        annots = rules.eggnogmapper.output[0],
        repeats = rules.edta.output[0],
        repeats_gff3 = rules.edta.output[1],
        repeats_sum = rules.edta.output[2]
    output:
        genome = f"{config['prefix']}_final/{config['prefix']}.genome.fasta",
        prots = f"{config['prefix']}_final/{config['prefix']}.proteins.fasta",
        transcripts = f"{config['prefix']}_final/{config['prefix']}.transcripts.fasta",
        cds = f"{config['prefix']}_final/{config['prefix']}.cds.fasta",
        gene_gff3 = f"{config['prefix']}_final/{config['prefix']}.genes.gff3",
        annots = f"{config['prefix']}_final/{config['prefix']}.annotations.txt",
        repeats = f"{config['prefix']}_final/{config['prefix']}.repeats.fasta",
        repeats_gff3 = f"{config['prefix']}_final/{config['prefix']}.repeats.gff3",
        repeats_sum = f"{config['prefix']}_final/{config['prefix']}.repeat_summary.txt",
    params:
        prefix = config['prefix'],
        outdir = f"{config['prefix']}_final",
        obo = f"{workflow.basedir}/scripts/go-basic.obo"
    threads:
        config['threads']
    conda:
        "envs/general.yml"
    script:
        "scripts/finalize_names.py"


