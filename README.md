# snake_annotation
This annotation pipeline is designed to use snakemake and tibanna to allow for annotating plant genomes using AWS. It will use [EDTA](https://github.com/oushujun/EDTA) to identify repeats, assembly long rna seq reads using [minimap2](https://github.com/lh3/minimap2) + [stringtie](https://github.com/gpertea/stringtie), map short rnaseq reads using [hisat2](https://github.com/infphilo/hisat2), build gene models using [funannotate](https://github.com/nextgenusfs/funannotate), and functionaly annotate the gene models using [Eggnog-Mapper V2](https://github.com/eggnogdb/eggnog-mapper/wiki/eggNOG-mapper-v2)

In order to run this pipeline using tibanna, you will need an aws account, at least one aws bucket, a working copy of snakemake, and will need to setup your tibanna unicorn. For details on how to setup tibanna, consult the [tibann docs](https://tibanna.readthedocs.io/en/latest/). For full details on execution options, consult the [snakemake docs](https://snakemake.readthedocs.io/en/stable/)

# Example Usage
To use this pipeline, begin by putting your genome fasta and any rna seq data you have into a single folder on your aws bucket. Next, clone this git repo to your computer. You then simply issue your snakemake command. The following series of commands might resemble what you need to do...

```
# Copy your genome and rna seq to s3
aws s3 cp sample.fasta s3://your-bucket/sample/sample.fasta
aws s3 cp sample.ont.fastq.gz s3://your-bucket/sample/sample.ont.fastq.gz
# clone this repo and move into the repo
git clone https://gitlab.com/salk-tm/snake_annotation.git
cd snake_annotation
# activate your environment which includes snakemake if needed
conda activate snake
# Issue your snakemake command to execute the pipeline on your sample
nohup snakemake -p --use-conda \
    --tibanna  --default-remote-prefix=your_bucket/sample/ \
    --default-resources mem_mb=65536 disk_mb=300000 \
    --config ref=sample.fasta busco=viridiplantae \
    lr=sample.ont.fastq.gz gene_prefix=FUN augustus_species=maize \
    --tibanna-config root_ebs_size=16 \
    > snake_annotation.log 2>&1 &
```

Note that we control our aws instance specifications using "--default-resources mem_mb=65536 disk_mb=300000" which will set our minimum instance memory at 64 gigs and mininum ebs disk size at about 300 gigs. This should be sufficient for most plants but may need to be adjusted depending on your needs. Due to the EDTA conda package size, you will also need to use "--tibanna-config root_ebs_size=16" in order to provide enough space on the instances root drive for the EDTA package to be downloaded during installation.

HINT : Use the "--dryrun" snakemake option to check your command before you try to run it. This will let you kind of see the commands that will be run by snakemake as part of execution and verify that the input files can be found by snakemake.

# Config options

This pipeline uses the standard snakemake config system to specify input files and control pipeline execution. The following fields are available...

## Required fields 
#### ref
This allows you to specify your genome fasta file which will be annotated

#### busco
This pipeline takes advantage of the busco training functionality of funannotate. You must provide the name of a busco database to use during gene model prediction. Acceptable values are: viridiplantae, chlorophyta, liliopsida, brassicales, eudicots, solanales, poales, embryophyta, fabales

#### gene_prefix
A prefix that will be used for your gene model IDs. This option makes it easier to keep your gene model ids unique across numerous samples making analysis between them more easy.

#### augustus_species
The augustus models used for initial busco gene prediction.


## Short read rnaseq data (Optional)
#### sra_short
If present, should be a comma seperated list SRA numbers referencing illumina paired end RNAseq data for your species of interest. These datasets will be downloaded as part of execution, mapped to the genome using hisat2, then passed onto funannotate to be used for gene model prediction. Example: sra_short=SRR1111111,SRR2222222

#### sr1
If present, should be a comma seperated list of file names for R1 reads from an illumina paired end RNAseq experiment found in your s3 folder. These file names should be paired with the R2 reads specified via the sr2 option.

#### sr2
If present, should be a comma seperated list of file names for R2 reads from an illumina paired end RNAseq experiment found in your s3 folder. These file names should be paired with the R1 reads specified via the sr1 option.

## Long read rnaseq data (Optional)

#### sra_long
If present, should be a comma seperated list SRA numbers referencing oxford nanopore or pacbio RNAseq data for your species of interest. These datasets will be downloaded as part of execution, mapped to the genome using minimap2 and assembled by stringtie. These transcript models from stringtie are then passed onto funannotate to be used for gene model prediction. Example: sra_long=SRR1111111,SRR2222222

#### lr
If present, should be a comma seperated list of file names for oxford nanopore or pacbio rnaseq data found in your s3 folder.

## Command Options (Optional)
Any time you use these, you should be very careful to check the resultant generated commands using the "--dryrun" option with snakemake.

#### predict_opt
This option allows you to modify the funannotate predict command responsible for producing gene models.

#### setup_opt
This option allows for execution of arbitrary commands prior to running the funannotate predict command. For example, you may want to download a specific protein database and pass this protein database to funannotate using the 'predict_opt' above

#### hisat2_opt
This option allows you to modify the hisat2 command used to map short reads to your reference.

#### eggnog_opt
This option allows you to modify the eggnog-mapper command. For example, you may wish to limit the gene ontology evfidence used to experimental only rather than the default of merely non-electronic. To do so, use "eggnog_opt=--go_evidence experimental". Or you may wish to limit the taxonomic scope of annotations to only a specific taxa, perhaps viridiplantae. To do so, you may use "eggnog_opt=--tax_scope 33090"

#### minimap2_opt
This option allows you to modify the minimap2 command used to map long reads to your reference. For example, if your reads data is stranded, you may want to use "minimap2_opt=-u f".

#### stringtie_opt
This option allows you to modify the stringtie command used to assemble the long read alignments from minimap2 into transcripts.

#### edta_opt
This option allows you to modify the EDTA command used to identify repeats in the genome.

